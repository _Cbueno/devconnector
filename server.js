const express = require("express");
const mongoose = require("mongoose");

const users = require("./routes/api/resourcesApi/users");
const profile = require("./routes/api/resourcesApi/profile");
const posts = require("./routes/api/resourcesApi/posts");
const app = express();

//DB config
const db = require("./config/keys").mongoURI;
//Connect to MongoDB
mongoose
  .connect(db)
  .then(() => console.log("Conectado no Mongo"))
  .catch(err => console.log(err));

app.get("/", (req, res) => res.send("olá de novo"));
//usar rotas configuradas
app.use("/api/users", users);
app.use("/api/profile", profile);
app.use("/api/posts", posts);

const port = process.env.port || 5002;
app.listen(port, () => console.log(`Servidor rodando na porta ${port}`));
